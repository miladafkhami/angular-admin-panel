# Angular Client Panel

A project build with Angular and AngularFire2.
This project implements OAUTH authentication with Firestore/Auth API.

## Live Demo [Here](https://clientt-panell.web.app)
