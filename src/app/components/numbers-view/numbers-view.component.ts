import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-numbers-view",
  templateUrl: "./numbers-view.component.html",
  styleUrls: ["./numbers-view.component.scss"],
})
export class NumbersViewComponent implements OnInit {
  @Input() data;
  constructor() {}

  ngOnInit() {}
}
