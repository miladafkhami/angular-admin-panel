import { Component, OnInit } from "@angular/core";
import { ChartType, ChartOptions } from "chart.js";
import { MultiDataSet, Label } from "ng2-charts";

@Component({
  selector: "app-pie-chart",
  templateUrl: "./pie-chart.component.html",
  styleUrls: ["./pie-chart.component.scss"],
})
export class PieChartComponent implements OnInit {
  public doughnutChartLabels: Label[] = ["Bad", "Good", "Normal"];
  public doughnutChartData: MultiDataSet = [[32, 32, 64]];
  public doughnutChartType: ChartType = "doughnut";
  options: ChartOptions = {
    circumference: 1 * Math.PI,
    rotation: Math.PI,
    cutoutPercentage: 50,
  };
  constructor() {}

  ngOnInit() {}
}
