import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthService } from "../../services/auth.service";
import { Router, NavigationStart } from "@angular/router";
import { FlashMessagesService } from "angular2-flash-messages";
import { SettingsService } from "../../services/settings.service";
import "rxjs/add/operator/filter";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit {
  isLoggedIn: boolean;
  loggedInUser: string;
  showRegister: boolean;
  selectedTab: string = "OVERVIEW";
  profileModalOpen: boolean = false;
  navbarTabs: string[] = ["OVERVIEW", "ANALYTICS", "DATA", "ALERTS"];

  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private settingsService: SettingsService
  ) {}

  ngOnInit() {
    this.router.events
      .filter((event) => event instanceof NavigationStart)
      .subscribe((event: NavigationStart) => {
        if (this.navbarTabs.indexOf(event.url.slice(1)) !== -1) {
          this.selectedTab = event.url.slice(1);
        }
      });

    this.authService.getAuth().subscribe((auth) => {
      if (auth) {
        this.isLoggedIn = true;
        this.loggedInUser = auth.email;
      } else {
        this.isLoggedIn = false;
      }
    });

    this.showRegister = this.settingsService.getSettings().allowRegistration;
  }

  onLogoutClick(e: Event) {
    e.preventDefault();
    this.authService.logout();
    this.profileModalOpen = this.isLoggedIn = false;
    this.flashMessage.show("You are now logged out", {
      cssClass: "alert-success",
      timeout: 4000,
    });
    this.router.navigate(["/login"]);
  }

  toggleProfileModal() {
    this.profileModalOpen = !this.profileModalOpen;
  }
}
