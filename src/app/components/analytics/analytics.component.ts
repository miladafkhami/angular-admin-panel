import { Component, OnInit, ViewChild } from "@angular/core";
import { ChartComponent } from "ng-apexcharts";
import { Row_ChartOptions } from "../../models/chart";

@Component({
  selector: "app-analytics",
  templateUrl: "./analytics.component.html",
  styleUrls: ["./analytics.component.scss"],
})
export class AnalyticsComponent implements OnInit {
  @ViewChild("chart", { static: true }) chart: ChartComponent;
  public chartOptions: Partial<Row_ChartOptions>;
  numbersData = [
    { title: "Total Alerts", value: "210", unit: "Alerts", rate: "+260" },
    { title: "Connected Devices", value: "320", of: "567", unit: "Devicce", rate: "-145" },
    { title: "Transmited Data", value: "450", unit: "GB", rate: "+325" },
  ];
  constructor() {}

  ngOnInit() {
    this.chartOptions = {
      series: [
        { name: "PRODUCT A", data: [44, 55, 41, 67, 22, 43, 21, 49] },
        { name: "PRODUCT B", data: [13, 23, 20, 8, 13, 27, 33, 12] },
      ],
      chart: { type: "line", height: 150, zoom: { enabled: false } },
      xaxis: { categories: [ "2011 Q1", "2011 Q2", "2011 Q3", "2011 Q4", "2012 Q1", "2012 Q2", "2012 Q3", "2012 Q4" ] },
    };
  }
}
