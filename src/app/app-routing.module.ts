import { AnalyticsComponent } from "./components/analytics/analytics.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

// Components:
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { LoginComponent } from "./components/login/login.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { RegisterComponent } from "./components/register/register.component";

// Guards:
import { AuthGuard } from "./guards/auth.guard";
import { RegisterGuard } from "./guards/register.guard";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/OVERVIEW",
    pathMatch: "full",
    canActivate: [AuthGuard],
  },
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "register",
    component: RegisterComponent,
    canActivate: [RegisterGuard],
  },
  {
    path: "OVERVIEW",
    component: DashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "ANALYTICS",
    component: AnalyticsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "DATA",
    component: AnalyticsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "ALERTS",
    component: AnalyticsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "**",
    component: NotFoundComponent,
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)],
  providers: [AuthGuard, RegisterGuard],
})
export class AppRoutingModule {}
