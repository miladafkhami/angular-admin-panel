import { NumbersViewComponent } from "./components/numbers-view/numbers-view.component";
import { AnalyticsComponent } from "./components/analytics/analytics.component";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/Forms";
import { FlashMessagesModule } from "angular2-flash-messages";

import { environment } from "../environments/environment";
import { AngularFireModule } from "angularfire2";
import { AngularFirestoreModule } from "angularfire2/firestore";
import { AngularFireAuthModule } from "angularfire2/auth";

import { AppComponent } from "./app.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { AppRoutingModule } from ".//app-routing.module";
import { AuthService } from "./services/auth.service";
import { SettingsService } from "./services/settings.service";
import { PieChartComponent } from "./components/pie-chart/pie-chart.component";
import { ChartsModule } from "ng2-charts";
import { BarChartComponent } from "./components/bar-chart/bar-chart.component";
import { NgApexchartsModule } from "ng-apexcharts";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DashboardComponent,
    SidebarComponent,
    LoginComponent,
    RegisterComponent,
    NotFoundComponent,
    AnalyticsComponent,
    NumbersViewComponent,
    PieChartComponent,
    BarChartComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlashMessagesModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase, "clientpanel"),
    AngularFirestoreModule,
    AngularFireAuthModule,
    ChartsModule,
    NgApexchartsModule,
  ],
  providers: [AuthService, SettingsService],
  bootstrap: [AppComponent],
})
export class AppModule {}
